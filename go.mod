module gitlab.com/idawson/youcallme

go 1.16

require (
	github.com/creack/pty v1.1.17
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/goterm v0.0.0-20200907032337-555d40f16ae2 // indirect
	golang.org/x/net v0.0.0-20211020060615-d418f374d309
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
)
